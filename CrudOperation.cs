﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoBezaoDalPay.EntityFrameWork;


namespace AutoBezaoDalPay
{
   public static class CrudOperation
   {
        public static int AddNewUser()
        {
            using (var context = new AutoBezaoEntities())
            {
                try
                {
                    var user = new EntityFrameWork.User() {Name = "Bobo", Email = "bobo@gmail.com"};
                    context.Users.Add(user);
                    context.SaveChanges();
                    Console.WriteLine("Add successful");
                    return user.Id;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException?.Message);
                    return 0;
                }
                
            }
           
        }
        public static void PrintAllUsers()
        {
           using (var context = new AutoBezaoEntities())
           {
                foreach (User user in context.Users)
                {
                    Console.WriteLine(user.Id.ToString() + " " + " " + user.Name + " "+ " " + user.Email);
                }
           } 
        }
        public static void DeleteFromUsers(int Id)
        {
            using (var context = new AutoBezaoEntities())
            {
                User userToDelete = context.Users.Find(Id);
                if (userToDelete != null)
                {
                    context.Users.Remove(userToDelete);
                    Console.WriteLine("User has been successfully deleted");

                    if (context.Entry(userToDelete).State != EntityState.Deleted)
                    {
                        throw new Exception("Unable to delete the record");
                    }
                    context.SaveChanges();
                }
                
            }
        }
        public static void UpdateUserRecord(int userId)
        {
            using (var context = new AutoBezaoEntities())
            {
               User userToUpdate = context.Users.Find(userId);
                if (userToUpdate != null)
                {
                    Console.WriteLine(context.Entry(userToUpdate).State);
                    userToUpdate.Name = "Obinze";
                    Console.WriteLine("Name of the user id has been updated");
                    Console.WriteLine(context.Entry(userToUpdate).State);
               
                context.SaveChanges();
                }
            }
        }
    }
}
